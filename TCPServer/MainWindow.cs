﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TCPServer
{
    public partial class MainWindow : Form
    {
        private ChatServer chatServer;
        private bool serverRunning = false;
        public string pageHTML;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            wbChat.Navigate("about:blank");
            wbChat.Document.OpenNew(false);
            pageHTML = ChatHTML.BasePage();
            wbChat.Document.Write(pageHTML);
            wbChat.Refresh();
        }

        private async void bStart_Click(object sender, EventArgs e)
        {
            bStart.Enabled = false;
            await Task.Run(() => {
                if(!serverRunning)
                {
                    startServer();
                } else
                {
                    stopServer();
                }
            });
        }

        private void startServer()
        {
            IPAddress address;
            IPEndPoint endPoint;
            try
            {
                address = IPAddress.Parse(tbAddress.Text);
                endPoint = new IPEndPoint(address, (int)nudPort.Value);
            }
            catch
            {
                MessageBox.Show("Błędny format adresu IP!");
                Invoke(new MethodInvoker(delegate {
                    tbAddress.Clear();
                    bStart.Enabled = true;
                }));
                return;
            }

            try
            {
                chatServer = new ChatServer(endPoint, this);
                chatServer.StartListening();

                serverRunning = true;
                Invoke(new MethodInvoker(delegate {
                    bStart.Text = "Wyłącz";
                    bStart.Enabled = true;
                    tbAddress.Enabled = false;
                    nudPort.Enabled = false;
                    tbInput.Enabled = true;
                    bSend.Enabled = true;

                    lbLog.Items.Add(DateTime.Now.ToString() + " | Uruchomiono serwer");
                }));
            }
            catch (Exception ex)
            {
                Invoke(new MethodInvoker(delegate { lbLog.Items.Add(DateTime.Now.ToString() + " | Błąd inicjacji serwera!"); }));
                MessageBox.Show(ex.ToString(), "Błąd!");
            }
        }

        private void stopServer()
        {
            chatServer.Stop();
            serverRunning = false;

            Invoke(new MethodInvoker(delegate {
                lbLog.Items.Add(DateTime.Now.ToString() + " | Zakończono pracę serwera");

                bStart.Text = "Włącz";
                bStart.Enabled = true;
                tbAddress.Enabled = true;
                nudPort.Enabled = true;
                tbInput.Enabled = false;
                tbInput.Clear();
                bSend.Enabled = false;

                wbChat.Document.Write(ChatHTML.BasePage());
                wbChat.Refresh();
                wbChat.Document.Body.ScrollIntoView(false);
            }));
        }

        private async void tbInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && tbInput.Text.Length > 0)
            {
                e.Handled = e.SuppressKeyPress = true;

                await Task.Run(() =>
                {
                    SendMessage();
                });
            }
        }

        private async void bSend_Click(object sender, EventArgs e)
        {
            if (tbInput.Text.Length > 0)
            {
                await Task.Run(() =>
                {
                    SendMessage();
                });
            }
        }

        private void SendMessage()
        {
            if (tbInput.Text.Length == 0) return;

            ChatMessage message = new ChatMessage(MessageType.Text, DateTime.Now, "Serwer", tbInput.Text.Replace("<", "&lt"));
            //chatServer.Broadcast(message);

            chatServer.HandleMessage(message);
            chatServer.BroadcastLog();

            //pageHTML += message.ChatForm();
            //pageHTML += ChatHTML.AddMessage(message, "Serwer");
            //Invoke(new MethodInvoker(delegate {
            //    wbChat.Document.Write(pageHTML);
            //    wbChat.Refresh();
            //    wbChat.Document.Body.ScrollIntoView(false);

            //    tbInput.Clear();
            //    tbInput.Select();
            //}));

            Invoke(new MethodInvoker(delegate {
                tbInput.Clear();
                tbInput.Select();
                ActiveControl = tbInput;
            }));

            //chatServer.chatLog.Add(message);
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Invoke(new MethodInvoker(delegate
            {
                wbChat.Document.Write(ChatHTML.BasePage());
                wbChat.Refresh();
                wbChat.Document.Body.ScrollIntoView(false);
            }));
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Invoke(new MethodInvoker(delegate {
                wbChat.Document.Write(ChatHTML.GenerateFromLog(chatServer.chatLog, "Serwer"));
                wbChat.Refresh();
                wbChat.Document.Body.ScrollIntoView(false);
            }));
        }
    }
}
