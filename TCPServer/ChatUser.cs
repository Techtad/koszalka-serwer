﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TCPServer
{
    public class ChatUser
    {
        private TcpClient client;
        private BinaryReader reader;
        private BinaryWriter writer;
        public string Username { get; set; }
        private bool connected = false;
        public UserSettings Settings { get; }

        public ChatUser(TcpClient tcpClient)
        {
            client = tcpClient;
            NetworkStream stream = client.GetStream();
            reader = new BinaryReader(stream);
            writer = new BinaryWriter(stream);
            connected = true;
            Settings = new UserSettings();
        }

        public ChatMessage Read()
        {
            return ChatMessage.Parse(reader.ReadString());
        }

        public void Write(ChatMessage msg)
        {
            writer.Write(msg.ToString());
        }

        public bool IsConnected()
        {
            return connected && client.Connected;
        }

        public void Disconnect()
        {
            connected = false;
            try { Dispose(); } catch { }
        }

        public void Dispose()
        {
            if (!client.Connected) return;
            client.GetStream().Close();
            client.Close();
        }
    }
}
