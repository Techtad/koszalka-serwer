﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCPServer
{
    public class UserSettings
    {
        private List<string> mutedUsers;
        public UserSettings()
        {
            mutedUsers = new List<string>();
        }

        public void MuteUser(string user)
        {
            if(!mutedUsers.Contains(user))
                mutedUsers.Add(user);
        }

        public void UnmuteUser(string user)
        {
            if (mutedUsers.Contains(user))
                mutedUsers.Remove(user);
        }

        public bool isMuted(string user)
        {
            return mutedUsers.Contains(user);
        }
    }
}
