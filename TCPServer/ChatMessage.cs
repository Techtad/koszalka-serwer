﻿using System;
using System.Globalization;

namespace TCPServer
{
    public enum MessageType
    {
        Text,
        Response,
        Info,
        UserName,
        UserList,
        ChatLog,
        Command,
        PrivateMessage,
        Unknown = 9
    }

    public class ChatMessage
    {
        public readonly MessageType Type;
        public readonly string Sender;
        public readonly string Text;
        public readonly DateTime Time;

        public ChatMessage(MessageType type, DateTime time, string sender, string text)
        {
            Type = type;
            Time = time;
            Sender = sender;
            Text = text/*.Replace("<", "&lt")*/;
        }

        override public string ToString()
        {
            return (int)Type + ";" + Time.ToString("ddMMyyyyHHmmss") + ";" + Sender + ";" + Text;
        }

        public string ChatForm()
        {
            return Time.ToString() + " | <b>" + Sender + ":</b> " + Text + "<br>";
        }

        //TODO: Zrobić to jakoś czyściej
        public static ChatMessage Parse(string data)
        {
            string[] parts = data.Split(';');

            MessageType type;
            DateTime time;
            string sender;
            string text;

            if(parts.Length >= 4)
            {
                if(parts[0].Length == 1)
                {
                    try
                    {
                        int typeIndex = int.Parse(parts[0]);
                        type = Enum.IsDefined(typeof(MessageType), typeIndex) ? (MessageType)typeIndex : MessageType.Unknown;
                    } catch
                    {
                        type = MessageType.Unknown;
                    }
                } else
                {
                    type = MessageType.Unknown;
                }

                if(parts[1].Length == 14)
                {
                    try
                    {
                        time = DateTime.ParseExact(parts[1], "ddMMyyyyHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None);
                    } catch
                    {
                        time = DateTime.Now;
                    }
                } else
                {
                    time = DateTime.Now;
                }

                if (parts[2].Length > 0)
                    sender = parts[2];
                else
                    sender = "Unknown";

                text = data.Substring(parts[0].Length + parts[1].Length + parts[2].Length + 3);
            } else
            {
                type = MessageType.Unknown;
                time = DateTime.Now;
                sender = "Unknown";
                text = data;
            }

            //text = text.Replace("<", "&lt");

            return new ChatMessage(type, time, sender, text);
        }
    }
}
