﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TCPServer
{
    public class ChatServer
    {
        private Dictionary<string, ChatUser> users;
        private TcpListener listener;
        private bool running;
        private Thread listenThread;
        private volatile object accessLock;
        private MainWindow app;
        public List<ChatMessage> chatLog;

        public ChatServer(IPEndPoint ip, MainWindow window)
        {
            users = new Dictionary<string, ChatUser>();
            listener = new TcpListener(ip);
            //listener.Server.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            running = false;
            accessLock = new object();
            app = window;
            chatLog = new List<ChatMessage>();
        }

        public void StartListening()
        {
            running = true;
            listenThread = new Thread(Listening);
            listenThread.Start();
        }

        private void Listening()
        {
            try
            {
                listener.Start();
                while (running)
                {
                    TcpClient client = listener.AcceptTcpClient();
                    Thread userThread = new Thread(() => HandleUser(new ChatUser(client)));
                    userThread.Start();
                }
            } catch(Exception e)
            {
                //MessageBox.Show(e.ToString());
                //app.Invoke(new MethodInvoker(delegate { app.lbLog.Items.Add(DateTime.Now.ToString() + " | " + e.ToString()); }));
            } finally
            {
                listener.Stop();
                lock (accessLock)
                {
                    foreach (ChatUser user in users.Values)
                        user.Disconnect();

                    users.Clear();
                    app.Invoke(new MethodInvoker(delegate { app.lbUsers.Items.Clear(); }));
                }
            }
        }

        public void Stop()
        {
            running = false;
            try { listener.Stop(); } catch { }
        }

        private string CheckUsername(string username)
        {
            if (username.Length == 0)
                return "Nazwa użytkownika nie może być pusta";
            else if (!username.All(char.IsLetterOrDigit))
                return "Nazwa użytkownika zawiera niedozwolone znaki";
            else if (users.Keys.Contains(username))
                return "Nazwa użytkownika zajęta";
            else if (username.ToUpper() == "SERWER" || username.ToUpper() == "SERVER")
                return "Niedozwolona nazwa użytkownika";
          
            return "Accept";
        }

        private void HandleUser(ChatUser user)
        {
            string username = user.Read().Text;
            string response = CheckUsername(username);
            user.Write(new ChatMessage(MessageType.Response, DateTime.Now, "Serwer", response));
            if (response != "Accept")
            {
                user.Disconnect();
                return;
            }
            user.Username = username;
            lock (accessLock)
            {
                users.Add(username, user);
                app.Invoke(new MethodInvoker(delegate { app.lbUsers.Items.Add(username); }));
                ChatMessage userJoinedMsg = new ChatMessage(MessageType.Info, DateTime.Now, "Serwer", "<div class='user'>" + username + "</div> dołączył do czatu");
                HandleMessage(userJoinedMsg);
                //Broadcast(userJoinedMsg);
                BroadcastLog();
                if (users.Count > 0) BroadcastUserList();
            }

            while (user.IsConnected())
            {
                try
                {
                    ChatMessage msg = user.Read();
                    lock (accessLock)
                    {
                        HandleMessage(msg);
                        //Broadcast(msg);
                        BroadcastLog();
                    }
                } catch
                {
                    DisconnectUser(user);
                }
            }
            user.Dispose();
        }

        private void DisconnectUser(ChatUser user)
        {
            user.Disconnect();
            ChatMessage userDisconnectedMsg = new ChatMessage(MessageType.Info, DateTime.Now, "Serwer", "<div class='user'>" + user.Username + "</div> wyszedł z czatu");
            lock (accessLock)
            {
                users.Remove(user.Username);
                app.Invoke(new MethodInvoker(delegate { app.lbUsers.Items.Remove(user.Username); }));
                HandleMessage(userDisconnectedMsg);
                //Broadcast(userDisconnectedMsg);
                BroadcastLog();
                if(users.Count > 0) BroadcastUserList();
            }
        }

        public void Broadcast(ChatMessage msg)
        {
            foreach(ChatUser user in users.Values)
            {
                if(user.Username != msg.Sender)
                {
                    try
                    {
                        user.Write(msg);
                    } catch { }
                }
            }
        }

        public void BroadcastUserList()
        {
            string payload = string.Empty;
            foreach (string name in users.Keys) payload += name + "|";
            payload = payload.Substring(0, payload.Length - 1);
            Broadcast(new ChatMessage(MessageType.UserList, DateTime.Now, "Serwer",  payload));
        }

        public void BroadcastLog()
        {
            foreach (ChatUser user in users.Values)
            {
                string payload = string.Empty;
                foreach (var msg in ChatLogForUser(user))
                    payload += new ChatMessage(msg.Type, msg.Time, msg.Sender, msg.Text.Replace("|", "&#124").Replace(";", "&#59")).ToString() + "|";

                payload = payload.Substring(0, payload.Length - 1);
                try
                {
                    user.Write(new ChatMessage(MessageType.ChatLog, DateTime.Now, "Serwer", payload));
                }
                catch { }
            }
            //Broadcast(new ChatMessage(MessageType.ChatLog, DateTime.Now, "Serwer", payload));
        }

        public void HandleMessage(ChatMessage msg)
        {
            if (msg.Type == MessageType.Command)
                HandleCommand(msg);
            else
            {
                chatLog.Add(msg);
                app.Invoke(new MethodInvoker(delegate
                {
                    app.wbChat.Document.Write(ChatHTML.GenerateFromLog(chatLog, "Serwer"));
                    app.wbChat.Refresh();
                    app.wbChat.Document.Body.ScrollIntoView(false);
                }));
            }
        }

        public void HandleCommand(ChatMessage msg)
        {
            string[] words = msg.Text.Split(' ');
            if (words.Length > 0)
            {
                string cmd = words[0];
                switch (cmd)
                {
                    case "mute":
                        if (words.Length > 1)
                        {
                            string arg = words[1];
                            if (users.Keys.Contains(msg.Sender))
                                users[msg.Sender].Settings.MuteUser(arg);
                        }
                        break;
                    case "unmute":
                        if (words.Length > 1)
                        {
                            string arg = words[1];
                            if (users.Keys.Contains(msg.Sender))
                                users[msg.Sender].Settings.UnmuteUser(arg);
                        }
                        break;
                    case "pm":
                        if (words.Length > 1)
                        {
                            string recipient = words[1];
                            if (words.Length > 2)
                            {
                                string message = msg.Text.Substring(words[0].Length + words[1].Length + 2);
                                HandleMessage(new ChatMessage(MessageType.PrivateMessage, msg.Time, msg.Sender, recipient + ";" + message));
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public List<ChatMessage> ChatLogForUser(ChatUser user)
        {
            List<ChatMessage> log = new List<ChatMessage>();

            foreach(var msg in chatLog)
            {
                if (user.Settings.isMuted(msg.Sender))
                    continue;

                if(msg.Type == MessageType.PrivateMessage)
                {
                    string recipient = msg.Text.Split(';')[0];
                    if (user.Username != recipient)
                        continue;
                }

                log.Add(msg);
            }

            return log;
        }
    }
}
