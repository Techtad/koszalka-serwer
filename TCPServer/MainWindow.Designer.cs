﻿namespace TCPServer
{
    partial class MainWindow
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.lAddress = new System.Windows.Forms.Label();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.lPort = new System.Windows.Forms.Label();
            this.nudPort = new System.Windows.Forms.NumericUpDown();
            this.lbLog = new System.Windows.Forms.ListBox();
            this.bStart = new System.Windows.Forms.Button();
            this.tbInput = new System.Windows.Forms.TextBox();
            this.wbChat = new System.Windows.Forms.WebBrowser();
            this.lbUsers = new System.Windows.Forms.ListBox();
            this.bSend = new System.Windows.Forms.Button();
            this.lUsers = new System.Windows.Forms.Label();
            this.lLog = new System.Windows.Forms.Label();
            this.cmsBrowser = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).BeginInit();
            this.cmsBrowser.SuspendLayout();
            this.SuspendLayout();
            // 
            // lAddress
            // 
            this.lAddress.AutoSize = true;
            this.lAddress.Location = new System.Drawing.Point(12, 18);
            this.lAddress.Name = "lAddress";
            this.lAddress.Size = new System.Drawing.Size(34, 13);
            this.lAddress.TabIndex = 0;
            this.lAddress.Text = "Adres";
            // 
            // tbAddress
            // 
            this.tbAddress.Location = new System.Drawing.Point(63, 14);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(100, 20);
            this.tbAddress.TabIndex = 1;
            this.tbAddress.Text = "127.0.0.1";
            // 
            // lPort
            // 
            this.lPort.AutoSize = true;
            this.lPort.Location = new System.Drawing.Point(178, 18);
            this.lPort.Name = "lPort";
            this.lPort.Size = new System.Drawing.Size(26, 13);
            this.lPort.TabIndex = 2;
            this.lPort.Text = "Port";
            // 
            // nudPort
            // 
            this.nudPort.Location = new System.Drawing.Point(210, 14);
            this.nudPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nudPort.Name = "nudPort";
            this.nudPort.Size = new System.Drawing.Size(120, 20);
            this.nudPort.TabIndex = 3;
            this.nudPort.Value = new decimal(new int[] {
            1234,
            0,
            0,
            0});
            // 
            // lbLog
            // 
            this.lbLog.FormattingEnabled = true;
            this.lbLog.HorizontalScrollbar = true;
            this.lbLog.Location = new System.Drawing.Point(631, 59);
            this.lbLog.Name = "lbLog";
            this.lbLog.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbLog.Size = new System.Drawing.Size(288, 355);
            this.lbLog.TabIndex = 4;
            // 
            // bStart
            // 
            this.bStart.Location = new System.Drawing.Point(345, 14);
            this.bStart.Name = "bStart";
            this.bStart.Size = new System.Drawing.Size(75, 20);
            this.bStart.TabIndex = 5;
            this.bStart.Text = "Włącz";
            this.bStart.UseVisualStyleBackColor = true;
            this.bStart.Click += new System.EventHandler(this.bStart_Click);
            // 
            // tbInput
            // 
            this.tbInput.Enabled = false;
            this.tbInput.Location = new System.Drawing.Point(12, 420);
            this.tbInput.Name = "tbInput";
            this.tbInput.Size = new System.Drawing.Size(327, 20);
            this.tbInput.TabIndex = 8;
            this.tbInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbInput_KeyDown);
            // 
            // wbChat
            // 
            this.wbChat.AllowNavigation = false;
            this.wbChat.AllowWebBrowserDrop = false;
            this.wbChat.CausesValidation = false;
            this.wbChat.ContextMenuStrip = this.cmsBrowser;
            this.wbChat.IsWebBrowserContextMenuEnabled = false;
            this.wbChat.Location = new System.Drawing.Point(15, 59);
            this.wbChat.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbChat.Name = "wbChat";
            this.wbChat.Size = new System.Drawing.Size(405, 355);
            this.wbChat.TabIndex = 9;
            this.wbChat.Url = new System.Uri("about:blank", System.UriKind.Absolute);
            this.wbChat.WebBrowserShortcutsEnabled = false;
            // 
            // lbUsers
            // 
            this.lbUsers.FormattingEnabled = true;
            this.lbUsers.HorizontalScrollbar = true;
            this.lbUsers.Location = new System.Drawing.Point(426, 59);
            this.lbUsers.Name = "lbUsers";
            this.lbUsers.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbUsers.Size = new System.Drawing.Size(199, 355);
            this.lbUsers.TabIndex = 10;
            // 
            // bSend
            // 
            this.bSend.Enabled = false;
            this.bSend.Location = new System.Drawing.Point(345, 420);
            this.bSend.Name = "bSend";
            this.bSend.Size = new System.Drawing.Size(75, 20);
            this.bSend.TabIndex = 11;
            this.bSend.Text = "Wyślij";
            this.bSend.UseVisualStyleBackColor = true;
            this.bSend.Click += new System.EventHandler(this.bSend_Click);
            // 
            // lUsers
            // 
            this.lUsers.AutoSize = true;
            this.lUsers.Location = new System.Drawing.Point(476, 43);
            this.lUsers.Name = "lUsers";
            this.lUsers.Size = new System.Drawing.Size(101, 13);
            this.lUsers.TabIndex = 12;
            this.lUsers.Text = "Lista Użytkowników";
            // 
            // lLog
            // 
            this.lLog.AutoSize = true;
            this.lLog.Location = new System.Drawing.Point(731, 43);
            this.lLog.Name = "lLog";
            this.lLog.Size = new System.Drawing.Size(90, 13);
            this.lLog.TabIndex = 13;
            this.lLog.Text = "Dziennik Zdarzeń";
            // 
            // cmsBrowser
            // 
            this.cmsBrowser.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearToolStripMenuItem,
            this.refreshToolStripMenuItem});
            this.cmsBrowser.Name = "cmsBrowser";
            this.cmsBrowser.Size = new System.Drawing.Size(181, 70);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.clearToolStripMenuItem.Text = "Wyczyść";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.refreshToolStripMenuItem.Text = "Odśwież";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 450);
            this.Controls.Add(this.lLog);
            this.Controls.Add(this.lUsers);
            this.Controls.Add(this.bSend);
            this.Controls.Add(this.lbUsers);
            this.Controls.Add(this.wbChat);
            this.Controls.Add(this.tbInput);
            this.Controls.Add(this.bStart);
            this.Controls.Add(this.lbLog);
            this.Controls.Add(this.nudPort);
            this.Controls.Add(this.lPort);
            this.Controls.Add(this.tbAddress);
            this.Controls.Add(this.lAddress);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Serwer Czatu";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudPort)).EndInit();
            this.cmsBrowser.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lAddress;
        private System.Windows.Forms.TextBox tbAddress;
        private System.Windows.Forms.Label lPort;
        private System.Windows.Forms.NumericUpDown nudPort;
        public System.Windows.Forms.ListBox lbLog;
        private System.Windows.Forms.Button bStart;
        private System.Windows.Forms.TextBox tbInput;
        public System.Windows.Forms.WebBrowser wbChat;
        public System.Windows.Forms.ListBox lbUsers;
        private System.Windows.Forms.Button bSend;
        private System.Windows.Forms.Label lUsers;
        private System.Windows.Forms.Label lLog;
        private System.Windows.Forms.ContextMenuStrip cmsBrowser;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
    }
}

