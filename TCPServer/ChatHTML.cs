﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace TCPServer
{
    public static class ChatHTML
    {
        public static string css = "* { margin: 0; padding: 0; box-sizing: border-box; } .segment { width: 100%; margin: 4px 0; overflow: hidden; } .text { width: 100%; padding: 4px; border: 1px solid black; overflow: hidden; } .timestamp { font-size: 75%; color: black; margin-top: 7px; } .sender { font-weight: bold; color: black; margin-top: 4px; } .me .text { background-color: dodgerblue; color: white; } .timestamp, .sender { float: left; } .sender { margin-right: 8px; } .me .timestamp, .me .sender { float: right; color: lightgray; } .me .sender { margin-right: 0; margin-left: 8px; color: white; } .user { font-weight: bold; display: inline; } .other .text { background-color: #dddddd; color: black; } .info .text, .server .text { border: 1px solid black; } .pm .text { background-color: #aaffaa; } .pm .sender { font-weight: normal; }";
        public static string BasePage()
        {
            return "<html><head><style>" + css + "</style></head><body onload='window.scrollTo(0,document.body.scrollHeight);'>";
        }

        public static string AddMessage(ChatMessage msg, string me)
        {
            string html = "<div class='segment ";

            switch (msg.Type)
            {
                case MessageType.Text:
                    html += msg.Sender == me ? "me" : msg.Sender == "Serwer" ? "server" : "other";
                    break;
                case MessageType.Info:
                    html += "info";
                    break;
                case MessageType.PrivateMessage:
                    html += "pm";
                    break;
                default:
                    return string.Empty;
            }
            if (msg.Type == MessageType.PrivateMessage)
                html += "'><div class='text'><pre>" + msg.Text.Substring(msg.Text.IndexOf(';') + 1) + "<br>";
            else html += "'><div class='text'><pre>" + msg.Text + "<br>";
            if (msg.Type == MessageType.Text) html += "<div class='sender'>" + msg.Sender + "</div>";
            else if (msg.Type == MessageType.PrivateMessage) html += "<div class='sender'>PM od <div class='user'>" + msg.Sender + "</div> do <div class='user'>" + msg.Text.Split(';')[0] + "</div></div>";
            html += "<div class='timestamp'>" + msg.Time.ToString() + "</div>";
            html += "</pre></div></div>";

            return html;
        }

        public static string GenerateFromLog(List<ChatMessage> log, string me)
        {
            string html = BasePage();
            foreach(var msg in log)
            {
                html += AddMessage(msg, me);
            }

            return html;
        }
    }
}
